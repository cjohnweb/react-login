import { merge } from 'lodash';

const UserReducer = (state = {}, action) => {
    let newState;
    switch (action.type) {
        case "RECEIVE_USER":
            newState = merge({}, state);
            return newState;
        default:
            return state;
    }
};

export default UserReducer;
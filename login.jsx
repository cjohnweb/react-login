import React from 'react';
import ReactDOM from 'react-dom';
import { Provider } from 'react-redux';
import store from './store';

// This is our main component
import Main from './components/main';

// Event listener is used only to load react AFTER the jade template has finished rendering the HTML in the browser.
window.addEventListener("DOMContentLoaded", () => {
    let root = document.getElementById("login");

    // Now lets render our main component
    ReactDOM.render(<Provider store={store}><Main /></Provider>, root);

});


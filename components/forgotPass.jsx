import React from 'react'; // Need this to do React.Component
import styles from './styles'; // Need this to apply styles to our jsx html
import { connect } from 'react-redux';

class LoginForm extends React.Component {

    constructor(props) {
        super(props);
        this.state = {
            userAuth: {
                email: ""
            }
        };

        this.updateAuth = this.updateAuth.bind(this);
        this.test = this.test.bind(this);
        this.log = this.log.bind(this);
    }


    updateAuth(e) {
        let field = e.target.dataset.field;
        let value = e.target.value;
        let userAuth = Object.assign({}, this.state.userAuth);
        userAuth[field] = value;
        console.log("updateAuth userAuth: ", userAuth);
        this.setState({ userAuth });
    }

    log(msg) {
        console.log("MSG: ", msg);
        alert("MSG: " + msg);
    }

    test() {
        console.log("TEST Email: ", this.state.userAuth.email);
        alert("Email: " + this.state.userAuth.email);
    }

    render() {

        let email = this.state.userAuth.email;

        return (
            <div { ...styles.general.page }>
                <div {...styles.general.loginBox}>
                    <div {...styles.general.loginBody}>
                        <span>HeartbeatIoT</span>
                        <h2>Forgot Password</h2>
                        <div style={{ padding: "25px", margin: "25px auto" }}>
                            <label {...styles.general.formLabel}>
                                <span {...styles.general.formText}>Email: </span>
                                <input autoFocus {...styles.general.formInput} data-field='email' name='email' value={email} onChange={this.updateAuth} />
                            </label>
                        </div>
                        <div style={{ padding: "25px", margin: "25px auto" }}>
                            <label {...styles.general.formLabel}>
                                <span {...styles.general.formText}></span>
                                <button {...styles.general.formLoginNext} onClick={this.test}>Next</button>
                            </label>
                        </div>
                    </div>
                    <div {...styles.general.footer}>
                        <div
                            style={{ float: "left", dispaly: "block", marginRight: "80px", cursor: "pointer" }}
                            data-page="loginForm"
                            onClick={this.props.changePage}
                        > Back To Login </div>
                    </div>
                </div >
            </div>
        );
    }
}

export default connect((state, ownProps) => ({
    changePage: ownProps.changePage
}), dispatch => ({
    userAuth: (userAuth) => dispatch({ type: "USER_AUTH" }),
}))(LoginForm);
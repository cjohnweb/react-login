import { style, nthChild } from 'glamor';
import modal from './modal_styles';

const styles = {
    modal: modal,
    buttons: {
        blueButton: style({
            transition: "all .5s ease",
            backgroundColor: "#0000ff",
            width: "130px",
            border: "3px solid #000000"
        }),
    },
    text: {
        loginTitle: style({
            marginTop: "6px",
            fontFamily: "'Futura Light', Arial",
            fontSize: "2rem",
            color: "#000000"
        }),
        loginSmallText: style({
            fontFamily: "'Futura Light', Arial",
            fontSize: ".9rem",
            color: "#000000"
        }),
    },
    slider: {
        container: style({
            position: "fixed",
            padding: "0",
            margin: "0",
            top: "0",
            left: "0",
            width: "100%",
            height: "100%",
            fontFamily: "'Futura Light', Arial",
            color: "#000000",
            backgroundSize: "cover",
            WebkitBackgroundSize: "cover",
            MozBackgroundSize: "cover",
            OBackgroundSize: "cover"
        }),
        main: style({
            width: "100%",
            height: "Calc(100% - 64px)",
            overflow: "hidden"
        }),
        slideContainer: style({
            height: "100%",
            width: "100%",
            overflow: "hidden",
            position: "fixed",
            top: "0",
            left: "0",
            width: "100%",
            height: "100%",
            zIndex: "1",
        }),
        slideStatic: style({
            height: "100%",
            width: "100%",
            margin: "0px",
            padding: "0px",
        }),
        slideRight: style({
            position: "relative",
            display: "grid",
            height: "100%",
            width: "200%",
            gridTemplateColumns: "50% 50%",
            gridTemplateRows: "100%",
            left: "-100%"
        }),
        slideLeft: style({
            position: "relative",
            display: "grid",
            height: "100%",
            width: "200%",
            gridTemplateColumns: "50% 50%",
            gridTemplateRows: "100%",
            left: "0"
        }),
        slideUp: style({
            position: "relative",
            display: "grid",
            height: "200%",
            width: "100%",
            gridTemplateRows: "50% 50%",
            gridTemplateColumns: "100%",
            top: "-100%"
        }),
        slideDown: style({
            position: "relative",
            display: "grid",
            height: "200%",
            width: "100%",
            gridTemplateRows: "50% 50%",
            gridTemplateColumns: "100%",
            top: "0%"
        })
    },
    general: {
        page: style({
            width: "100%",
            margin: "0px",
            padding: "0px",
            display: "flex",
            justifyContent: "center",
            alignItems: "center",
            align: "center",
            fontFamily: "'Futura Light', Arial",
            transition: "all 0.5s ease",
        }),
        loginBox: style({
            justifyItems: "center",
            marginTop: "3%",
            padding: "40px 40px 20px 36px",
            width: "450px",
            height: "500px",
            border: "1px solid #000000",
            borderRadius: ".1rem",
            boxShadow: "2px 2px 5px #888888",
            overflowY: "auto",
            display: "block",
            overflow: "hidden",
            transition: "300ms",
            color: "#000000",
            background: "#efefefdd",
            fontFamily: "'Futura Light', Arial"
        }),
        formLabel: style({
            display: "block",
            // border: "1px solid #ff0000",
            fontFamily: "'Futura Light', Arial",
            fontSize: "1.1rem",
            margin: "0px 0px 20px 0px",
            padding: "0px"
        }),
        formText: style({
            fontSize: ".9rem",
            lineHeight: "1.5",
            width: "172px",
            display: "block",
            clear: "both"
        }),
        inputSpanText: style({
            display: "block",
            width: "100%",
            fontSize: "10px",
            color: "#7bb9f7",
            margin: "0px 0px 4px 0px",
            padding: "0px"

        }),
        formInput: style({
            marginTop: "0px",
            display: "block",
            color: "#000000",
            border: "0px solid #000000",
            borderBottom: "1px solid #000000",
            background: "transparent",
            // boxShadow: "0px 4px #666666",
            outline: "0",
            fontFamily: "'Futura Light'",
            fontSize: "1.2rem",
            fontWeight: "bold",
            width: "100%",
            padding: "3px",
            paddingLeft: "10px",
            "::placeholder": {
                color: "#555555"
            }
            // borderBottomRightRadius: "1em",
            // borderBottomLeftRadius: "1em",
            // borderTopRightRadius: "1em",
            // borderTopLeftRadius: "1em",
            // marginLeft: "20px"
        }),
        formLoginNext: style({
            marginTop: "0px",
            marginLeft: "19px",
            display: "block",
            border: "0",
            outline: "0",
            borderBottom: "0px solid #4B4B4B",
            fontFamily: "'Futura Light'",
            fontSize: "1rem",
            width: "150px",
            borderBottomRightRadius: ".2em",
            borderBottomLeftRadius: ".2em",
            borderTopRightRadius: ".2em",
            borderTopLeftRadius: ".2em",
            transition: "background-color 0.5s ease",
            backgroundColor: "#7bb9f7",
            ":hover": {
                backgroundColor: "#2792fd"
            }
        }),
        loginBody: style({
            height: "Calc(100% - 100px)"
        }),
        footer: style({
            fontFamily: "'Futura Light', Arial",
            fontDecoration: "bold",
            fontSize: "1rem",
            color: "#333333",
            height: "50px",
            width: "100%",
        }),
    },

    container: style({
        height: "100%",
        width: "100%",
        marginBottom: "40px"
    }),
    pageContainer: style({
        width: "1024px",
        margin: "0 auto",
    }),
    main: style({
        width: "1024px",
        margin: "0 auto",
    }),
    section: style({
        width: "100%"
    }),
    hidden: style({
        display: "none"
    }),
    searchFilterInputContainer: style({
        display: "flex",
        justifyContent: "row",
        textAlign: "left",
        width: "1024px",
        margin: "0px auto",
        paddingTop: "11px",
        paddingBottom: "14px",
        border: "0px",
        lineHeight: "1.3rem"
    }),
    searchFilterLabel: style({
        width: "200px",
        lineHeight: "1.7rem",
        fontSize: "1.2rem"
    }),
    searchFilter: style({
        paddingLeft: "10px",
        width: "400px",
        lineHeight: "1.7rem",
        fontSize: "1.2rem",
        border: "1px solid #000000",

    }),
    widgetListItem: style({
        width: "100%"
    }),
    deviceListTable: style({
        width: "1024px",
        border: "0px solid #000000"
    }),
    deviceContainer: style({
        border: "1px solid #000000"
    }),
    clearBoth: style({
        width: "100%",
        display: "block",
        clear: "both"
    }),
    highlighter: style({
        backgroundColor: "#ff0000"
    }),
    clearTextLabel: style({
        color: "#adadad",
        fontSize: ".9rem",
        lineHeight: "1.5",
        width: "100px",
        display: "block",
    }),
    clearTextValue: style({
        color: "#000000",
        fontSize: ".9rem",
        lineHeight: "1.5",
        width: "250px",
        display: "block",
    }),
    clearText: style({
        lineHeight: "1.5",
        fontSize: "1rem"
    }),
    altTableTRs: style(
        nthChild('even', {
            backgroundColor: "#ffffff",
            cursor: "pointer",
            transition: "background-color 0.25s ease",
            ":hover": {
                backgroundColor: "#0096ff"
            }
        }),
        nthChild('odd', {
            backgroundColor: "#f6f6f6",
            cursor: "pointer",
            transition: "background-color 0.25s ease",
            ":hover": {
                backgroundColor: "#0096ff"
            }
        })
    ),
    rowOutOfDate: style({
        backgroundColor: "#fff0f0",
        cursor: "pointer",
        transition: "background-color 0.25s ease",
        ":hover": {
            backgroundColor: "#0096ff"
        }
    }),

    alignRight: style({
        textAlign: "right"
    }),
    navbar: style({
        width: "100%",
        height: "60px",
        borderBottom: "1px solid #222",
        backgroundColor: "white",
        display: "flex",
        justifyContent: "center",
        alignItems: "center"
    }),
    myNav: style({
        width: "100%",
        height: "30px",
        borderBottom: "1px solid #000000",
        backgroundColor: "#efefef",
        display: "flex",
        justifyContent: "center",
        alignItems: "center"
    }),
    myNavLi: style({
        display: "inline-block",
        paddingRight: "20px",
        listStyle: "none",
        cursor: "pointer",
    }),
    logo: style({
        fontSize: "20px"
    }),
    statusDot: style({
        width: "16px",
        height: "16px",
        margin: "0 auto",
    }),
    centerStatusDot: style({
        margin: "0px auto"
    }),
    groupSummaries: style({
        alignSelf: "flex-start",
        width: "400px",
        marginRight: "40px"
    }),
    leftIndent: style({
        marginLeft: "20px"
    }),
    strike: style({
        textDecoration: "line-through"
    }),
    verticalContainer: style({
        display: "flex",
        justifyContent: "flex-start",
        alignItems: "center",
        marginLeft: "200px",
        //clear:"both",
        width: "100%",
        marginTop: "40px",
        marginBottom: "40px",
        //height:"auto",
        //display:"block"
    }),

    buttons: style({
        margin: "20px 0",
        width: "100%",
        display: "flex",
        justifyContent: "space-around",
        alignItems: "center"
    }),
    button: style({
        width: "100px",
        height: "40px",
        display: "flex",
        justifyContent: "center",
        alignItems: "center",
        backgroundColor: "#ccc",
        cursor: "pointer",
        transition: "background-color 0.25s ease",
        ":hover": {
            backgroundColor: "#ddd"
        }
    }),
    tableContainer: style({
        width: "100%",
        display: "flex",
        justifyContent: "center"
    }),
    table: style({
        border: "1px solid #ccc",
        borderCollapse: "collapse"
    }),
    row: style({
        width: "100%",
        border: "1px solid #000000",
        display: "flex",
        flexDirection: "row",
        jsutifyContent: "center"
    }),
    device3rdColumn: style({
        width: "340",
        borderRight: "1px solid #000000",
        padding: "4px"
    }),
    rowOffline: style({
        backgroundColor: "#adadad",
        cursor: "pointer",
        transition: "background-color 0.25s ease",
        ":hover": {
            backgroundColor: "#ff5757"
        }
    }),
    cell: style({
        padding: "5pm",
        border: "1px solid #ccc"
    }),
    info: style({
        padding: "10px"
    }),
    paragraph: style({
        marginBottom: "10px"
    }),
    form: style({
        padding: "10px",
        display: "flex",
        flexDirection: "column"
    }),
    label: style({
        display: "flex",
        flexDirection: "right",
        // border: "1px solid #ff0000",
        fontFamily: "'Futura Light'",
        fontSize: "1.1rem",
        margin: "0px 0px 10px 0px",
        padding: "0px",
        // ":last-child": {
        //   margin: "0"
        // }
    }),
    input: style({
        padding: "5px"
    }),
    textarea: style({
        padding: "5px",
        width: "300px",
        height: "100px"
    }),
    footer: style({
        width: "100%",
        height: "60px",
        borderTop: "1px solid #222",
        color: "#222",
        fontSize: "10px",
        display: "flex",
        justifyContent: "center",
        alignItems: "center",
        backgroundColor: "white"
    })
};

export default styles;
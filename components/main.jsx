import React from 'react';
import { connect } from 'react-redux';
import styles from './styles';


// Main Navigational "Pages" / Components
import LoginForm from './loginForm';
import ForgotPass from './forgotPass';
import NewAccount from './newAccount';

class LoginInterface extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      page: "loginForm",
      next: null,
      opacity: 0,
      opacity2: 0,
      background: "greybackground.jpg",
      config: {
        smallLogoURL: "/login/assets/logo-black.svg",
        sitename: "HeartbeatIoT.com",
      }
    };

    this.sort = {
      loginForm: 0,
      forgotPass: 1,
      newAccount: 2
    };

    this.changePage = this.changePage.bind(this);
    // this.rotate = this.rotate.bind(this);

  }

  componentDidMount() {
    // document.addEventListener("keydown", this.rotate, false);
    window.setTimeout(() => {
      this.setState({ opacity: "1" });
    }, 10);

    window.setTimeout(() => {
      this.setState({ opacity2: "1" });
    }, 500);

  }

  componentDidUpdate() {
    if (this.state.next) {
      window.setTimeout(() => {
        this.refs.slider.style.transition = "left 0.5s ease";
        this.refs.slider.style.left = (this.sort[this.state.page] < this.sort[this.state.next] ? "-100%" : "0");
      }, 20);
      window.setTimeout(() => {
        //this.refs.slider.style.removeProperty('transition');
        this.refs.slider.style.removeProperty('left');
      }, 520);
    }
  }

  changePage(e) {
    let page = e.currentTarget.dataset.page;
    if (!this.state.next && this.state.page !== page) {
      this.setState({ next: page });
      window.setTimeout(() => {
        this.setState({ page: page, next: null });
      }, 520);
    }
  }

  render() {
    let slideStyle;
    let loginForm;
    let forgotPass;
    let newAccount;

    if (this.state.page === "loginForm" || this.state.next === "loginForm") {
      loginForm = (
        <LoginForm changePage={this.changePage} config={this.state.config} />
      );
    }

    if (this.state.page === "forgotPass" || this.state.next === "forgotPass") {
      forgotPass = (
        <ForgotPass changePage={this.changePage} config={this.state.config} />
      );
    }

    if (this.state.page === "newAccount" || this.state.next === "newAccount") {
      newAccount = (
        <NewAccount changePage={this.changePage} config={this.state.config} />
      );
    }

    if (this.state.next) {
      slideStyle = (this.sort[this.state.page] < this.sort[this.state.next] ? styles.slider.slideLeft : styles.slider.slideRight);
    } else {
      slideStyle = styles.slider.slideStatic;
    }

    let content = (
      <div ref="slider" {...slideStyle}>
        {loginForm}
        {forgotPass}
        {newAccount}
      </div>
    );

    let time = Date.now();

    return (
      <div {...styles.slider.container} style={{
        transition: "all 0.5s ease",
        opacity: this.state.opacity,
        backgroundColor: "#ffffff",
        backgroundSize: "100% auto",
        background: `url(/login/assets/${this.state.background}) no-repeat center center fixed`,
      }}>
        <div style={{ transition: "all 2s ease", opacity: this.state.opacity2 }}>
          {content}
        </div>
      </div>
    );
  }
}

export default connect((state, ownProps) => ({
  // user: state.user
}), dispatch => ({
  // userAuth: (userAuth) => dispatch({ type: "USER_AUTH" }),
}))(LoginInterface);;


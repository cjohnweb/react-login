import React from 'react'; // Need this to do React.Component
import styles from './styles'; // Need this to apply styles to our jsx html
import { connect } from 'react-redux';

class LoginForm extends React.Component {

    constructor(props) {
        super(props);
        this.state = {
            userAuth: {
                email: "",
                password: ""
            }
        };

        this.inputs = {};

        this.updateAuth = this.updateAuth.bind(this);
        this.doSignIn = this.doSignIn.bind(this);
        this.log = this.log.bind(this);
        this.enterTabInput = this.enterTabInput.bind(this);
    }

    componentDidMount() {
        this.inputs.email.focus();
        document.addEventListener("keydown", this.enterTabInput, false);
    }

    componentWillUnmount() {
        document.removeEventListener("keydown", this.enterTabInput, false);
    }

    componentWillUpdate(nextProps, nextState) {

    }

    updateAuth(e) {
        let field = e.target.dataset.field;
        let value = e.target.value;
        let userAuth = Object.assign({}, this.state.userAuth);
        userAuth[field] = value;
        this.setState({ userAuth });
    }

    enterTabInput(event) {
        if (event.key == 'Enter') {
            event.preventDefault();
            this.doSignIn(); // Submit the form
        }
        // if (event.key == 'Tab') {
        //     event.preventDefault();
        //     /* look at field and determine which field to focus on next */
        //     if (event.dataset.field == "email") {
        //         this.inputs.password.focus();
        //     } else if (event.dataset.field == "password") {
        //         this.inputs.email.focus();
        //     }
        // } 
    }

    log(msg) {
        console.log("MSG: ", msg);
        alert("MSG: " + msg);
    }

    doSignIn() {
        alert("Email: ", this.state.userAuth.email);
        console.log("Submit signin info to signIn dispatch.\n" +
            "If correct credentials, update state with a redirect URL.\n" +
            "If incorect credentials, update state with error object.\n" +
            "error: {invalidAttempts: #, lockedOutUntil: epoch/null, reason: wrong password, username doesn't exist}\n");
    }

    render() {

        let email = this.state.userAuth.email;
        let password = this.state.userAuth.password;
        //console.log("RENDER Email: ", email);
        return (
            <div { ...styles.general.page }>
                <div { ...styles.general.loginBox }>

                    <div {...styles.general.loginBody}>
                        <span {...styles.text.loginSmallText}><i className="fa fa-sign-in"></i>&nbsp; Please sign-in to continue to</span><br />

                        <span {...styles.text.loginTitle}>
                            &nbsp; <img src={this.props.config.smallLogoURL} style={{ width: "25px", paddingTop: "10px", paddingRight: "10px" }} />
                            {this.props.config.sitename}</span>

                        <div style={{ padding: "25px 25px 6px 25px", margin: "30px auto 0px auto" }}>
                            <label {...styles.general.formLabel}>
                                <span {...styles.general.inputSpanText}>Email</span>
                                <input type="text" autoFocus style={{ width: "90%" }} {...styles.general.formInput}
                                    data-field='email' name='email' value={email}
                                    onChange={this.updateAuth}
                                    ref={ref => {
                                        this.inputs.email = ref;
                                    }} />
                            </label>
                            <label {...styles.general.formLabel}>
                                <span {...styles.general.inputSpanText}>Password</span>
                                <input type="password" style={{ width: "90%" }} {...styles.general.formInput}
                                    data-field='password' name='password' value={password}
                                    onChange={this.updateAuth}
                                    ref={ref => {
                                        this.inputs.password = ref;
                                    }} />
                            </label>

                        </div>
                        <div style={{ padding: "5px", margin: "5px auto" }}>
                            <label {...styles.general.formLabel} style={{ /*border: "1px solid #000000"*/ }}>
                                <button {...styles.general.formLoginNext} onClick={this.doSignIn}>Sign In</button>
                            </label>
                        </div>

                    </div>

                    <div {...styles.general.footer}>

                        <div {...styles.buttons.blueButton}>
                            <div style={{ float: "left", dispaly: "block", marginLeft: "26px", marginRight: "80px", cursor: "pointer" }}
                                data-page="forgotPass"
                                onClick={this.props.changePage}>Forgot Password?</div>

                        </div>
                        <div {...styles.buttons.blueButton}>
                            <div
                                style={{ float: "left", dispaly: "block", cursor: "pointer" }}
                                data-page="newAccount"
                                onClick={this.props.changePage}> New Account </div>
                        </div>

                    </div>
                </div >
            </div >
        );
    }
}

export default connect((state, ownProps) => ({
    changePage: ownProps.changePage,
    config: ownProps.config
}), dispatch => ({
    validateAccount: (userAuth) => dispatch({ type: "VALIDATE_ACCOUNT" }),
}))(LoginForm);
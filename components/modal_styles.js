import { style, nthChild } from 'glamor';

const modal = {
  main: style({
    width: "100%",
    height: "Calc(100% - 64px)",
    overflow: "hidden"
  }),
  header: style({
    fontSize: "2rem",
    fontWeight: "bold"
  }),
  clearTextLabel: style({
    color: "#adadad",
    fontSize: ".9rem",
    lineHeight: "1.5",
    width: "130px",
    display: "block",
  }),
  clearText: style({
    lineHeight: "1.5",
    fontSize: "1rem"
  }),
  regularList: style({
    marginLeft: "26px",
    lineHeight: "1.5rem",
  }),
  ulBullets: style({
    listStyle: "disc",
    // listStyleType: "disc"
  }),
  clearTextLabelMiddle: style({
    color: "#adadad",
    fontSize: ".9rem",
    lineHeight: "100px",
    width: "130px"
  }),
  systemsTextarea: style({
    display: "block",
    width: "Calc(100% - 182px)",
    height: "200px",
    margin: "0px auto",
  }),
  noticeBox: style({
    width: "95%",
    height: "75%",
    overflow: "auto",
    border: "1px solid #ccc",
    backgroundColor: "#eee",
    color: "#444",
    padding: "10px",
    marginBottom: "20px"
  }),
  smallBox350: style({
    padding: "10px",
    margin: "10px auto",
    width: "350px"
  }),
  smallBox450: style({
    padding: "10px",
    margin: "10px auto",
    width: "450px"
  }),
  container: style({
    position: "fixed",
    top: "0",
    left: "0",
    width: "100%",
    height: "100%",
    zIndex: "20",
    fontFamily: "'Futura Light', Arial",
    color: "rgb(150, 150, 150)"
  }),
  backdrop: style({
    height: "100%",
    width: "100%",
    backgroundColor: "rgba(0, 0, 0, 0.3)",
    transition: "opacity 0.5s ease",
    "@media(max-width: 1400px)": {
      display: "none"
    }
  }),
  window: style({
    position: "fixed",
    zIndex: "2",
    backgroundColor: "white",
    width: "1050px",
    height: "700px",
    top: "50%",
    left: "50%",
    transform: "translate(-50%, -50%)",
    transition: "opacity 0.5s ease",
    boxShadow: "0 10px 20px 0 rgba(0, 0, 0, 0.8)",
    "@media(max-width: 1400px)": {
      height: "100%",
      width: "100%",
      top: "0",
      left: "0",
      transform: "none",
      boxShadow: "none"
    }
  }),
  navBar: style({
    height: "60px",
    width: "100%",
    display: "flex",
    justifyContent: "space-between",
    borderBottom: "2px solid #D0D0D0",
  }),
  tabs: style({
    display: "flex",
    alignSelf: "flex-end",
    alignItems: "flex-end"
  }),
  tab: style({
    fontSize: "1.5rem",
    padding: "5px 6px 7px 6px",
    margin: "0 27px",
    borderBottom: "3px solid transparent",
    cursor: "pointer",
    transition: "color 1s ease, border 0.5s ease",
    "@media(max-width: 650px)": {
      margin: "0 10px"
    },
    "@media(max-width: 400px)": {
      fontSize: "1.25rem",
      margin: "0 5px"
    }
  }),
  selectedTab: style({
    borderBottom: "3px solid rgba(0, 150, 255, 0.8)",
    color: "#0096FF"
  }),
  closeButton: style({
    fontSize: "1.9rem",
    color: "#969696",
    cursor: "pointer",
    margin: "14px 20px",
    alignSelf: "flex-start"
  }),
  static: style({
    height: "100%",
    width: "100%"
  }),
  slideRight: style({
    position: "relative",
    display: "grid",
    height: "100%",
    width: "200%",
    gridTemplateColumns: "50% 50%",
    gridTemplateRows: "100%",
    left: "-100%"
  }),
  slideLeft: style({
    position: "relative",
    display: "grid",
    height: "100%",
    width: "200%",
    gridTemplateColumns: "50% 50%",
    gridTemplateRows: "100%",
    left: "0"
  }),
  slideContainer: style({
    height: "100%",
    width: "100%",
    overflow: "hidden"
  }),
  slidingStatic: style({
    height: "100%",
    width: "100%"
  }),
  slideUp: style({
    display: "grid",
    gridTemplateRows: "50% 50%",
    height: "200%",
    width: "100%",
    position: "relative",
    top: "-100%"
  }),
  slideDown: style({
    display: "grid",
    gridTemplateRows: "50% 50%",
    height: "200%",
    width: "100%",
    position: "relative",
    top: "0%"
  }),
  content: style({
    height: "Calc(100% - 28px)",
    color: "#4b4b4b"
  }),
  card: style({
    marginTop: "20px",
    height: "Calc(100% - 80px)",
    width: "100%",
    overflow: "auto"
  }),
  systemLogException: style({
    display: "block",
    width: "100%",
    padding: "20px",
    marginTop: "45px"
  }),
  companyProfilCard: style({
    height: "Calc(100% - 310px)",
    width: "100%",
    overflow: "auto"
  }),
  profileHeader: style({
    fontSize: "2rem",
    fontWeight: "bold",
    align: "center",
    justify: "center",
    marginBottom: "10px"
  }),
  finalizeButtonContainer: style({
    width: "40%",
    display: "flex",
    justifyContent: "center"
  }),
  companyButtonsSexy: style({
    backgroundColor: "#efefef",
    marginTop: "0px",
    marginBottom: "10px",
    width: "100%",
    display: "flex",
    justifyContent: "center",
    borderBottomLeftRadius: "2em",
    borderBottomRightRadius: "2em"
  }),
  button: style({
    margin: "0 10px",
    backgroundColor: "#ddd",
    display: "flex",
    alignItems: "center",
    justifyContent: "center",
    color: "#646464",
    width: "140px",
    height: "35px",
    fontSize: "1.1rem",
    fontWeight: "bold",
    cursor: "pointer",
    WebkitBorderRadius: "6px",
    MozBorderRadius: "6px",
    borderRadius: "6px",
    transition: "background-color 0.5s ease",
    ":hover": {
      backgroundColor: "#bbb"
    }
  }),
  buttons: style({
    marginTop: "12px",
    width: "100%",
    display: "flex",
    justifyContent: "center"
  }),
  buttonBase: style({
    transition: "all 500ms ease 500ms",
  }),
  buttonSexy: style({
    margin: "0 10px",
    display: "flex",
    alignItems: "center",
    justifyContent: "center",
    color: "#646464",
    minWidth: "130px",
    height: "20px",
    fontSize: ".9rem",
    fontWeight: "bold",
    cursor: "pointer",
    borderTopLeftRadius: ".25em",
    borderBottomRightRadius: ".25em",
    transition: "background-color 500ms ease 0ms",
    ":hover": {
      transition: "background-color 500ms ease 0ms",
      backgroundColor: "#bbb"
    }
  }),
  buttonSelectedSexy: style({
    margin: "0 10px",
    display: "flex",
    alignItems: "center",
    justifyContent: "center",
    color: "#646464",
    minWidth: "130px",
    height: "20px",
    fontSize: ".9rem",
    fontWeight: "bold",
    cursor: "pointer",
    borderTopLeftRadius: ".25em",
    borderBottomRightRadius: ".25em",
    backgroundColor: "#9dd8ff",
    // ":hover": {
    //   transition: "background-color 500ms ease 500ms",
    //   backgroundColor: "#9dd8ff",
    // }
  }),
  buttonRed: style({
    margin: "0 10px",
    backgroundColor: "#ff7272",
    display: "flex",
    alignItems: "center",
    justifyContent: "center",
    color: "#646464",
    width: "130px",
    height: "35px",
    fontSize: "1.1rem",
    fontWeight: "bold",
    cursor: "pointer",
    transition: "background-color 0.5s ease",
    ":hover": {
      backgroundColor: "#bbb"
    }
  }),
  buttonDisabled: style({
    margin: "0 10px",
    backgroundColor: "#999999",
    display: "flex",
    alignItems: "center",
    justifyContent: "center",
    color: "#888888",
    width: "130px",
    height: "35px",
    fontSize: "1.1rem",
    fontWeight: "bold",
    transition: "background-color 0.5s ease"
  }),
  buttonRedDisabled: style({
    margin: "0 10px",
    backgroundColor: "#999999",
    display: "flex",
    alignItems: "center",
    justifyContent: "center",
    color: "#888888",
    width: "130px",
    height: "35px",
    fontSize: "1.1rem",
    fontWeight: "bold",
    transition: "background-color 0.5s ease"
  }),
  buttonSelected: style({
    margin: "0 10px",
    backgroundColor: "#9dd8ff",
    display: "flex",
    alignItems: "center",
    justifyContent: "center",
    color: "#646464",
    width: "130px",
    height: "35px",
    fontSize: "1.1rem",
    fontWeight: "bold",
    cursor: "pointer",
    transition: "background-color 0.5s ease",
    ":hover": {
      backgroundColor: "#9de5ff"
    }
  }),
  pressed: style({
    backgroundColor: "#bbb",
    cursor: "default"
  }),
  hidden: style({
    display: "none"
  }),
  columns: style({
    display: "flex",
    alignItems: "flex-start",
    justifyContent: "center"
  }),
  // prefsColumns: style({
  //   display: "grid",
  //   gridTemplateColumns: "33% 33% 33%",
  //   margin: "0 auto"
  // }),
  formHeader: style({
    display: "block",
    marginTop: "44px",
    marginRight: "38px",
    marginBottom: "20px",
    marginLeft: "38px",
    fontSize: "1.3em",
    fontWeight: "bold"
  }),
  form: style({
    width: "100%",
    // marginRight: "20px"
    // border: "1px solid #000000"
  }),
  formShort: style({
    width: "300px",
    marginRight: "20px"
    // border: "1px solid #000000"
  }),
  formFatMargin: style({
    width: "450px",
    marginRight: "40px"
    // border: "1px solid #000000"
  }),
  label: style({
    display: "flex",
    flexDirection: "right",
    // border: "1px solid #ff0000",
    fontFamily: "'Futura Light'",
    fontSize: "1.1rem",
    margin: "0px 0px 10px 0px",
    padding: "0px",
    // ":last-child": {
    //   margin: "0"
    // }
  }),
  labelWarn: style({
    display: "flex",
    flexDirection: "right",
    backgroundColor: "#ffabab",
    // border: "1px solid #ff0000",
    fontFamily: "'Futura Light'",
    fontSize: "1.1rem",
    margin: "0px 0px 10px 0px",
    padding: "0px",
    // ":last-child": {
    //   margin: "0"
    // }
  }),
  labelHeader: style({
    display: "flex",
    flexDirection: "right",
    borderBottom: "1px solid #adadad",
    fontFamily: "'Futura Light'",
    fontSize: "1.2rem",
    margin: "0px 0px 20px 0px",
    padding: "0px",
  }),
  labelFatPadding: style({
    display: "flex",
    flexDirection: "row",
    fontFamily: "'Futura Light'",
    fontSize: "1.1rem",
    paddingBottom: "46px",
    paddingLeft: "20px",
    width: "100%"
  }),
  labelShort: style({
    display: "flex",
    flexDirection: "right",
    // border: "1px solid #ff0000",
    fontFamily: "'Futura Light'",
    fontSize: "1.1rem",
    margin: "0px 0px 10px 0px",
    padding: "0px",
    width: "80%"
    // ":last-child": {
    //   margin: "0"
    // }
  }),
  labelShortNoPadding: style({
    display: "flex",
    flexDirection: "right",
    fontFamily: "'Futura Light'",
    fontSize: "1.1rem",
    width: "80%"
  }),
  // input: style({
  //   marginTop: "0px",
  //   display: "block",
  //   border: "0",
  //   outline: "0",
  //   borderBottom: "1px solid #4B4B4B",
  //   fontFamily: "'Futura Light'",
  //   fontSize: "1.1rem",
  //   // paddingLeft: "20px",
  //   width: "60%",
  // }),
  inputSexy: style({
    marginTop: "0px",
    display: "block",
    border: "0",
    outline: "0",
    borderBottom: "1px solid #4B4B4B",
    fontFamily: "'Futura Light'",
    fontSize: "1rem",
    width: "200px",
    // marginLeft: "20px"
  }),
  inputSexySuperShort: style({
    marginTop: "0px",
    display: "block",
    border: "0",
    outline: "0",
    borderBottom: "1px solid #4B4B4B",
    fontFamily: "'Futura Light'",
    fontSize: "1rem",
    width: "40px",
    // marginLeft: "20px"
  }),
  logoInput: style({
    marginTop: "0px",
    display: "block",
    border: "0",
    outline: "0",
    borderBottom: "1px solid #4B4B4B",
    fontFamily: "'Futura Light'",
    fontSize: ".9rem",
    paddingLeft: "20px",
    width: "60%",
  }),
  // altInput: style({
  //   marginTop: "2px",
  //   border: "0",
  //   outline: "0",
  //   borderBottom: "1px solid #4B4B4B",
  //   fontFamily: "'Futura Light'",
  //   fontSize: "1.1rem",
  //   width: "100%",
  //   display: "flex",
  //   justifyContent: "space-between",
  //   alignItems: "center",
  //   cursor: "text",
  // }),
  // none: style({
  //   fontSize: "1.1rem",
  //   outline: "0",
  //   border: "0",
  //   fontFamily: "'Futura Light'",
  //   display: "inline-block",
  //   width: "100%"
  // }),
  notifyUserMessage: style({
    display: "block",
    textAlign: "center",
    width: "100%",
    padding: "3px",
    border: "1px solid #000000",
    backgroundColor: "#ffffff",
    borderBottomLeftRadius: ".25em",
    borderBottomRightRadius: ".25em",
    borderTopLeftRadius: ".25em",
    borderTopRightRadius: ".25em",
    boxShadow: "5px 5px 15px #ffe0e0",
  }),
  tableContainer: style({
    // height: "100%",
    // width: "100%",
    overflow: "visible",
    padding: "0px 30px 10px 10px"
  }), // Remove tableContainerPrefSolar and rename references to tableContainer instead
  tableContainerPrefSolar: style({
    // height: "100%",
    // width: "100%",
    overflow: "visible",
    padding: "0px 30px 10px 10px"
  }),
  tableContainerUsers: style({
    width: "Calc(100% - 20px)",
    overflow: "auto",
    padding: "0px 10px",
    display: "flex",
    justifyContent: "center"
  }),
  tableSysDevicesEditShort: style({
    width: "60%",
    borderCollapse: "collapse",
    fontSize: "1.1rem"
  }),
  tr: style(
    nthChild('even', {
      backgroundColor: "#ffffff",
      transition: "background-color 0.25s ease",
      ':hover': {
        backgroundColor: "#e6e6e6"
      }
    }),
    nthChild('odd', {
      backgroundColor: "#f8f8f8",
      transition: "background-color 0.25s ease",
      ':hover': {
        backgroundColor: "#e6e6e6"
      }
    })
  ),
  trBillingStartDates: {
    backgroundColor: "#ffffff",
    transition: "background-color 0.25s ease",
    ':hover': {
      backgroundColor: "#e6e6e6"
    }
  },
  trObsolete: style({
    transition: "background-color 0.25s ease",
    backgroundColor: "#eeeeee",
    ':hover': {
      backgroundColor: "#c7c7c7"
    }
  }),
  GreenHighlighted: style({
    backgroundColor: "#c8fbc8"
  }),
  tablePrefStorage: style({
    width: "Calc(100% - 20px)",
    borderCollapse: "collapse",
    fontSize: "1rem"
  }),
  cellPrefStorage: style({
    textAlign: "center",
    padding: "2px",
    border: "0px solid #000000",
    lineHeight: "1.1rem",
  }),
  cellPrefStorageLeft: style({
    marginLeft: "10px",
    textAlign: "left",
    padding: "2px",
    borderTop: "0px solid #000000",
    borderRight: "1px solid #efefef",
    borderBottom: "0px solid #000000",
    borderLeft: "0px solid #000000",
    lineHeight: "1.1rem",
  }),
  cellSystemsBillingStartDates: style({
    textAlign: "center",
    padding: "0px",
  }),
  cellPrefStorageAlignLeft: style({
    textAlign: "left",
    padding: "2px",
    border: "0px solid #000000",
    width: "50px"
  }),
  tableHeaderLeftAlign: style({
    padding: "6px",
    textAlign: "left",
    border: "0px solid #000000"
  }),
  tableHeaderMiddleAlign: style({
    padding: "6px",
    textAlign: "center",
    border: "0px solid #000000"
  }),
  tableCellAction: style({
    padding: "6px",
    textAlign: "center",
    border: "0px solid #000000",
    width: "100px",
    justifyContent: "center"
  }),
  tableCellLeftAlign: style({
    marginLeft: "10px",
    textAlign: "left",
    padding: "2px",
    borderTop: "0px solid #000000",
    borderRight: "1px solid #efefef",
    borderBottom: "0px solid #000000",
    borderLeft: "0px solid #000000",
    lineHeight: "1.1rem",
  }),
  tableCellMiddleAlign: style({
    margin: "0px 10px 0px 10px ",
    textAlign: "center",
    padding: "2px",
    borderTop: "0px solid #000000",
    borderRight: "1px solid #efefef",
    borderBottom: "0px solid #000000",
    borderLeft: "0px solid #000000",
    lineHeight: "1.1rem",
  }),
  trHeadPrefStorage: style({
    padding: "6px",
    border: "0px solid #000000"
  }),
  cellPrefUtility: style({
    textAlign: "center",
    padding: "0px",
    border: "1px solid #000000"
  }),
  border: style({
    border: "1px solid #000000"
  }),
  logoContainer: style({
    margin: "20px 0",
    width: "100%",
    textAlign: "center",
    justifyContent: "center"
  }),
  systemsContactsColumns: style({
    display: "flex",
    alignItems: "flex-start",
    justifyContent: "center",
    borderBottom: "0px solid #aaaaaa"
  }),
  smallNoticeText: style({
    color: "#adadad",
    fontSize: ".85rem",
    lineHeight: "1.5",
    width: "100%",
    display: "block",

  }),
  miniButton: style({
    cursor: "pointer",
    color: "#119020",
    ":hover": {
      color: "#16af28"
    }
  }),
  miniButtonPurple: style({
    cursor: "pointer",
    color: "#8E588F",
    ":hover": {
      color: "#c47cc5"
    }
  }),
  miniButtonGrey: style({
    cursor: "pointer",
    color: "#bbbbbb",
    ":hover": {
      color: "#cccccc"
    }
  }),
  miniButtonRed: style({
    cursor: "pointer",
    color: "#db1f1f",
    ":hover": {
      color: "#ff2424"
    }
  }),
  miniButtonDisabled: style({
    cursor: "pointer",
    color: "#bbbbbb",
    ":hover": {
      color: "#cccccc"
    }
  })
};

export default modal;

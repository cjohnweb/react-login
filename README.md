# React Login Component
John Minton <cjohnweb@gmail.com>
![Alt text](/loginScreenshot.png?raw=true "Login Screenshot")

## Still in Development

This is a fontend login component written in React (Javascript).

To use, you need only configure and tweak:
* Point a route to the routes.js file.
* In the app.js file point the data routes to your API for user authentication.
* Change the background image URL in main.jsx. Static assests (files) are stored in the /assets/ folder.
* Configure smallLogoURL and sitename in main.jsx's this.state.config
* Tweak the file to meet your needs as you see fit.
* Works in React 16 and 18.
* Note that package.json is missing. Sorry about that, I didn't realize until recently.
import { takeEvery, call, put, select } from 'redux-saga/effects';
import { userAuth } from '../api/authentication';
import { merge } from 'lodash';


function* doAuth(action) {
    let auth = yield call(userAuth, action.userAuth);
    if (auth.error) {
        alert("ERROR: " + (auth.error.detail || JSON.stringify(auth.error)));
    } else {
        yield put({ type: "REQUEST_USER", user: auth });
    }
}

function* watchDoAuth() {
    yield takeEvery("REQUEST_USERAUTH", doAuth);
}

function* UsersSaga() {
    yield [
        watchDoAuth()
    ];
}

export default UsersSaga;
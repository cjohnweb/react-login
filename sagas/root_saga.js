import AuthSaga from './authSaga';
import { takeEvery, put, call, select } from 'redux-saga/effects';
import { authentication } from '../api/authentication';

function* RootSaga() {
    yield [
        AuthSaga(),
    ];
}

export default RootSaga;
export function userAuth(user) {
  return fetch(`/api/auth/login`, {
    method: "POST",
    credentials: "same-origin",
    headers: {
      "Content-Type": "application/json"
    },
    body: JSON.stringify(user)
  }).then(response => response.json());
}

export function forgotPass(user) {
  return fetch(`/api/auth/forgotPass`, {
    method: "POST",
    credentials: "same-origin",
    headers: {
      "Content-Type": "application/json"
    },
    body: JSON.stringify(user)
  }).then(response => response.json());
}

export function newAccount(user) {
  return fetch(`/api/auth/newAccount`, {
    method: "POST",
    credentials: "same-origin",
    headers: {
      "Content-Type": "application/json"
    },
    body: JSON.stringify(user)
  }).then(response => response.json());
}
